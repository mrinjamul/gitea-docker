# gitea with docker-compose

## How to Use?

To start the server,

First clone the repository.

Then,

```sh
docker-compose pull
docker-compose up -d
```

To stop this server,

```sh
docker-compose stop
```

Removing the container,

```sh
docker-compose rm -f
```
